//
//  ViewController.m
//  Login
//
//  Created by Ciaran Slattery on 1/26/17.
//  Copyright © 2017 Ciaran Slattery. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UILabel *notificationLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.username = @"username";
    self.password = @"password";
    
    self.passwordTextField.secureTextEntry= YES;
    
    //[self.username stringByAppendingString:@"username"];
    //[self.password stringByAppendingString:@"password"];
    
    //NSLog(@"%@%@", self.username, self.password);
}

- (IBAction)loginWasPressed:(id)sender {
    
    BOOL isUsersEqual = [self.username isEqualToString:[self.usernameTextField text]];
    BOOL isPasswordsEqual =[self.password isEqualToString:[self.passwordTextField text]];
    
    if(isUsersEqual && isPasswordsEqual){
        NSLog(@"Successful login");
        [self.notificationLabel setText:@"Congrats! You have logged in!"];
    }
    else{
        NSLog(@"Failure");
        [self.notificationLabel setText:@"Your username or password is incorrect"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

@end
